/*
===============================================================================
Name        : 5-Proyecto.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

unsigned int volatile *const T0IR=(unsigned int *)0x40004000;
unsigned int volatile *const T0TCR=(unsigned int *)0x40004004;
unsigned int volatile *const T0PR=(unsigned int *)0x4000400C;
unsigned int volatile *const T0MCR=(unsigned int *)0x40004014;
unsigned int volatile *const T0MR0=(unsigned int *)0x40004018;

unsigned int volatile *const PCONP=(unsigned int *)0x400FC0C4;
unsigned int volatile *const ISER0=(unsigned int *)0xE000E100;
unsigned int volatile *const PINSEL0=(unsigned int *)0x4002C000;
unsigned int volatile *const AD0CR=(unsigned int *)0x40034000;
unsigned int volatile *const AD0GDR=(unsigned int *) 0x40034004;
unsigned int volatile *const AD0INTEN =(unsigned int *)0x4003400C;

unsigned int volatile *const FIO2SET=(unsigned int *)0x2009C058;
unsigned int volatile *const FIO2CLR=(unsigned int *)0x2009C05C;
unsigned int volatile *const FIO2DIR=(unsigned int *)0x2009C040;

unsigned int volatile * const PINSEL1 = (unsigned int *)0x4002C004;
unsigned int volatile * const DACR = (unsigned int *)0x4008C000;
int main(void) {
	*FIO2DIR|=0b1111;

	/////////////////CONFIGuRACION ADC///////////////////////////
	*PCONP |=(1<<12);//enciende el periferico
   //ClK por defecto es CCLK/4
	*PINSEL1 |=0b01<<14; //AD0.0 habilitado en pin P0.0
	*AD0CR =1<<0;//configuro el canal 0
	*AD0CR |=1<<21;//habilitCOIN DEL PERIFERICO
	*AD0INTEN = 1<<0;//habilito la interrupcion por canal 7
	*AD0INTEN &= ~(1<<8);//habilito las interrupcoines individuales x canal.
	*ISER0|=1<<22;//habilito interrrupcion en NVIC
   /////////CONFIGURACION DAC///////////////7
	*PINSEL1|=0b10<<20;
	//CCLK/4 por defecto

	*T0PR =0x00;//Prescaler resetea en 0
	*T0MCR=0b000011; //activo interrupcion por MATC0 y MATCH1 y resetea en Match1
	*T0MR0=25000;//valor de match0
	*ISER0|=0b10;//habilito timer0 en nvic
	*T0TCR=0b01;//habilito la cuenta
    while(1) {
    	//for(int j =200000;j>0;j--){}
    }
    return 0 ;
}
void TIMER0_IRQHandler(void){
	*AD0CR |=1<<24;//lanzo el conversor
}

void ADC_IRQHandler(void){
	/**FIO2CLR=0b1111;
	int valor = (*AD0GDR>>4)&&0b111111111111;

	if(valor<1024){*FIO2SET=0b1;}
	else if(valor<2048){*FIO2SET=0b11;}
	else if(valor<3072){*FIO2SET=0b111;}
	else {*FIO2SET=0b1111;}
	*/
	//registro de 12 bits
	int valor = (*AD0GDR>>6)&&0b111111111111;
	*DACR=valor<<6;//registro de 10 bits

}
