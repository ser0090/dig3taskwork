/*
===============================================================================
 Name        : SumadorDisplay.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
/*
#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif
*/
//#include <cr_section_macros.h>


// TODO: insert other include files here
/*#define FIO0DIR ((unsigned int*) 0x2009C000)
#define FIO2DIR ((unsigned int*) 0x2009C040)

#define FIO0PIN ((unsigned int volatile*) 0x2009C014)
#define FIO0SET ((unsigned int*) 0x2009C018)
#define FIO0CLR ((unsigned int*) 0x2009C01C)
#define FIO2SET ((unsigned int*) 0x2009C058)
#define FIO2CLR ((unsigned int*) 0x2009C05C)*/
// TODO: insert other definitions and declarations here
#define DYSP0 10
#define DYSP1 11
unsigned int volatile *const FIO0DIR=0x2009C000;
unsigned int volatile *const FIO0PIN=0x2009C014;
unsigned int volatile *const FIO0SET=0x2009C018;
unsigned int volatile *const FIO0CLR=0x2009C01C;
unsigned int volatile *const FIO2SET=0x2009C058;
unsigned int volatile *const FIO2CLR=0x2009C05C;
unsigned int volatile *const FIO2DIR=0x2009C040;

int main(void) {

	unsigned int tabla[]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};//LOGICA POSITIVA
	unsigned int suma;
	unsigned int dys;

	*FIO2DIR = 0X2FF;//REGISTRO2 COMO SALIDA
	*FIO0DIR |=(0b11)<<10;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        suma=(~(*FIO0PIN>>23))& 0b1111;//PINES 23 24 25 26
        suma=suma+((~(*FIO0PIN>>6))& 0b1111);//pines 6 7 8 9

        int disp0=suma/10;//divide los valores en dos digitos decimales
        suma=suma%10;

        *FIO0SET = (1 << DYSP1);
        *FIO2CLR = 0x7F;//PONGO EN ALTO TODAS LAS SALIDAS
        *FIO0CLR = (1 << DYSP0);
        *FIO2SET=tabla[suma];//utilizo los primero bit del registro fiodir 2. se carga en CLR xq los displays son activos por bajo


        for(int i=1000;i>0;i--){}

        *FIO0SET = (1 << DYSP0);
        *FIO2CLR = 0x7F;//PONGO EN CERO TODAS LAS SALIDAS
        *FIO0CLR = (1 << DYSP1);
        *FIO2SET=tabla[disp0];
        for(int i=1000;i>0;i--){}

    }
    return 0 ;
}

