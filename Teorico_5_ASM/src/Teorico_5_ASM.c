/*
===============================================================================
 Name        : Teorico_5_ASM.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
extern void dysplay_out(int i);
unsigned int volatile *const FIO0SET=(unsigned int *)0x2009C018;
unsigned int volatile *const FIO0CLR=(unsigned int *)0x2009C01C;
unsigned int volatile *const FIO0DIR=(unsigned int *)0x2009C000;
unsigned int volatile *const FIO2DIR=(unsigned int *)0x2009C040;
unsigned int volatile *const FIO2SET=(unsigned int *)0x2009C058;
unsigned int volatile *const FIO2CLR=(unsigned int *)0x2009C05C;
// TODO: insert other include files here

// TODO: insert other definitions and declarations here

int main(void) {

    // TODO: insert code here
	*FIO2DIR = 0x7F;//REGISTRO2 COMO SALIDA
	//*FIO2SET = 0x7F;
	// Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
    	dysplay_out(i);
    	for (int var = 0; var < 5000000; ++var) {}
        i=(i+1)%10 ;
    }
    return 0 ;
}
