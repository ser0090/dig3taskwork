.syntax unified
.global dysplay_out
.type dysplay_out, function

dysplay_out:	LDR	R1,=dysp //cargo la direccion de dysp en r1 // sin = es el contenido de la direccion

				ADD	R0,R1,R0,LSL#2 //suma r0=r1+r0*4 (r0<<2) // se le desplaza 2 por que se lo almacena cada 2

				LDR R0,[R0]   // cargo el contenido de la direcciona la que apunta r0
				LDR R1,FIOSET
				MOV R2,#0x7F
				STR R2,[R1]
				LDR R1,FIOCLR
				STR R0,[R1]
				MOV pc,lr


FIOSET:		.word	0x2009C058 //0x2009C018
FIOCLR:		.word	0x2009C05C //0x2009C01C

dysp:		.word   0x0000003F
one:		.word   0x00000006
two:		.word   0x0000005B
three:		.word   0x0000004F
four:		.word   0x00000066
five:		.word   0x0000006D
six:		.word   0x0000007D
seven:		.word   0x00000007
eigth:		.word   0x0000007F
nine:		.word   0x0000006F

.end
