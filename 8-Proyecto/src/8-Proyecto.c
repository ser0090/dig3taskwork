/*
===============================================================================
 Name        : 8-Proyecto.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
unsigned int volatile *const PCONP=(unsigned int *)0x400FC0C4;
unsigned int volatile *const U3DLL =(unsigned int *)0x4009C000; //0:7 MLB
unsigned int volatile *const U3DLM  =(unsigned int *)0x4009C004;//0:7
unsigned int volatile *const U3IER  =(unsigned int *)0x4009C004;

unsigned int volatile *const ISER0=(unsigned int *)0xE000E100;
unsigned int volatile *const PINSEL0=(unsigned int *)0x4002C000;
unsigned int volatile *const PINSEL1=(unsigned int *)0x4002C004;
unsigned int volatile *const PINMODE0 = (unsigned int *)0x4002C040;

unsigned int volatile *const PCLKSEL1=(unsigned int *)0x400FC1AC;

unsigned int volatile *const U3LSR = (unsigned int *) 0x4009C014;
unsigned int volatile *const U3LCR=(unsigned int *)0x4009C00C;

unsigned int volatile *const U3THR = (unsigned int *)0x4009C000;
unsigned int volatile *const U3RBR = (unsigned int *) 0x4009C000;

unsigned int volatile *const U0FCR = (unsigned int *)0x4000C008;
uint8_t datos [2];
uint8_t ack = 0b0000110;
uint8_t nak = 0b0010101;
uint8_t count = 0;
int main(void) {

	*PCONP |= (1<<25); //PCUART0 UART3 power/clock control bit. and desabited UART1
	*PCONP &= ~(3<<3);//desabilito uart 0,1
	*PCLKSEL1 |= (1<<18);
	*U3LCR = 0x03; // palabra 8 bits
	*U3LCR |= (1<<2); // bit de parada
	*U3LCR |= 0b10000000; //habilito el latch para configurar
    *U3DLL = 54; //0b10100001 ; // 9600
    *U3DLM =0;

    *U3LCR &=~(1<<7);//desabilito el latch



    *PINSEL0 =0b1010; // configurar los pines port 0
    //pin 0 TXD0 pin 1 RXD0 puerto 0
    *PINMODE0 = 0; // pin a pull up

    *U3IER = 1; // habilito la interrupcion por Recive Data Aviable


    *ISER0 |= 1<<8; //activate interrup uart3
    //-----------------------------------------------

    while(1) {

    }
    return 0 ;
}

void UART3_IRQHandler(void){
	while((*U3LSR & (1<<5))==0){}
	for (int i=1000;i>0;i--){}
	datos[count] = *U3RBR;
	//nak = *U3RBR;
	*U3THR = datos[count];
	count= (count+1) % 2;
}

