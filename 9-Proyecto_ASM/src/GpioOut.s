.syntax unified
.global gpio_out
.type gpio_out, function

gpio_out:	LDR	R1,FIODIR
			STR	R0,[R1]
			MOV pc, lr

FIODIR:		.word	0x2009c000
.end
