.syntax unified
.global led_off
.type led_off, function

led_off:	LDR	R1,FIOCLR
			STR	R0,[R1]
			MOV pc, lr

FIOCLR:		.word	0x2009c01c
