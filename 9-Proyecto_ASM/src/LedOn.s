.syntax unified
.global led_on
.type led_on, function

led_on:	LDR	R1,FIOSET
		STR	R0,[R1]
		MOV pc, lr

FIOSET:		.word	0x2009c018
