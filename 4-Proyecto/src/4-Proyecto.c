

#define LED2 22
unsigned int volatile *const FIO0DIR=(unsigned int *)0x2009C000;
unsigned int volatile *const FIO0PIN=(unsigned int *)0x2009C014;
unsigned int volatile *const FIO0SET=(unsigned int *)0x2009C018;
unsigned int volatile *const FIO0CLR=(unsigned int *)0x2009C01C;

unsigned int volatile *const ISER0=(unsigned int *)0xE000E100; //registro para habilitar las interrupciones
unsigned int volatile *const T0IR=(unsigned int *)0x40004000; //registro de interrupciones timer0
unsigned int volatile *const T0TCR=(unsigned int *)0x40004004;//control del contador del timer
unsigned int volatile *const T0TC=(unsigned int*) 0x40004008;//registro del contador del timer0
unsigned int volatile *const T0PR=(unsigned int *)0x4000400C;//preescaleer del tm0
unsigned int volatile *const T0MCR=(unsigned int *)0x40004014;//usado para controlar las interrpuciones
unsigned int volatile *const T0MR0=(unsigned int *)0x40004018;//match registre 0
unsigned int volatile *const T0MR1=(unsigned int *)0x4000401C;//match registre 1

// TODO: insert other definitions and declarations here

int main(void) {
	*FIO0DIR |= (1 << LED2);//configuro puerto0
	*FIO0SET = (1 << LED2);

	//CCLK/4 por defecto
	*T0PR=0;//Prescaler resetea en 0

   *T0MCR=0b011001; //activo interrupcion por MATC0 y MATCH1 y resetea en Match1 tabla 430
   *T0MR0=500000;//valor de match0
   *T0MR1=1000000;//valor de match1
   *ISER0|=0b10;//habilito timer0 en nvic
   *T0TCR=0b01;//habilito la cuenta
   while(1);
}

void TIMER0_IRQHandler(void){
	if((*T0IR&0b01)){
		*T0IR|=(0b1<<0);
		*FIO0SET = (1 << LED2);
	}
	else{
		*T0IR|=(0b1<<1);
		*FIO0CLR = (1 << LED2);
	}
}
